/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.sport12;

/**
 *
 * @author OS
 */
public class TestSport {

    public static void main(String[] args) {
        Car khim = new Car("Khim", 10, 100);
        khim.activity();
        khim.Target(200);
        khim.Target(500, 1000);

        Run kaew = new Run("Kaew", 50, 8000);
        kaew.activity();
        kaew.Target(500);
        kaew.Target(10, 80000);
        
        Car thuch = new Car("Thuch", 15, 300);
        thuch.activity();
        thuch.Target(500);
        thuch.Target(300, 700);
        
        Run arthit = new Run("Arthit", 30, 1000);
        arthit.activity();
        arthit.Target(5000);
        arthit.Target(30, 100);
    }
}
