/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.sport12;

/**
 *
 * @author OS
 */
public class Run extends Sport {

    public Run(String Athlete, int Trophy, int Distance) {
        super(Athlete, Trophy, Distance);
    }

    public void activity() {
        System.out.println("Hi Im Runner My name is " + this.Athlete + ". get Trophy " + this.Trophy + " Bowl.  Distance is " + this.Distance + " Km");
    }

    public void Target(int Trophy, int Distance) {
        if (this.Trophy >= Trophy || this.Distance >= Distance) {
            System.out.println("Your target Trophy and Distance is bad!!");
        } else {
            this.Distance = Distance;
            this.Trophy = Trophy;
            System.out.println("My target of Runner , I have to practice to get " + this.Distance + " Km. for " + this.Trophy + " Trophy");
        }
    }

    public void Target(int Distance) {
        if (this.Distance >= Distance) {
            System.out.println("Your target Distance is bad!!");
        } else {
            this.Distance = Distance;
            System.out.println("My target of Runner , I have to practice to get " + this.Distance + " Km.");
        }
    }
}
