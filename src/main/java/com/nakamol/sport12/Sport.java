/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.sport12;

/**
 *
 * @author OS
 */
public class Sport {

    protected String Athlete;
    protected int Trophy;
    protected int Distance;

    public Sport(String Athlete, int Trophy, int Distance) {
        this.Athlete = Athlete;
        this.Trophy = Trophy;
        this.Distance = Distance;
    }

    public void activity() {
        System.out.println(this.Athlete + " get Trophy " + this.Trophy + "  Distance is " + this.Distance);
    }

    public void Trophy() {
        System.out.println(this.Athlete + " get Trophy " + this.Trophy);
    }

    public void Target(int Trophy, int Distance) {
        System.out.println("My target , I have to practice to get" + this.Distance + " for " + this.Trophy + " Trophy");
    }

    public void Target(int Distance) {
        System.out.println("My target , I have to practice to get" + this.Distance);

    }

    public void setAthlete(String Athlete) {
        this.Athlete = Athlete;
    }

    public void setTrophy(int Trophy) {
        this.Trophy = Trophy;
    }

    public void setDistance(int Distance) {
        this.Distance = Distance;
    }
}
